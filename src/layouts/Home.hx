package layouts;

import react.React;
import react.ReactDOM;
import react.ReactComponent;

import components.rails.Main;

/**
  Component for the Home layout
**/
class Home extends react.ReactComponent {
  /**
    Render home layout element
  **/
  override function render() : ReactElement {
    return React.createElement(Rails, { groupId: 'home' });
  }
}
