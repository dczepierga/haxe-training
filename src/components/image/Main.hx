package components.image;

import react.React;
import react.ReactDOM;
import react.ReactComponent;

import Config;
import utils.api.Main;
import components.image.Model;

/**
  Component for creating image
**/
class Image extends react.ReactComponentOfProps<ImageProps> {
  public function new(props : ImageProps) : Void {
    super(props);
  }
  
  /**
    Generate image url based on image id and mimetype
  **/
  private function getImageUrl(id, mimeType) : String {
    var baseUrl : String = Api.getServiceUrl('img', Config.serviceImgVersion);
    var params : String = 'Quality=95&Width=460&Height=194&ResizeAction=%27fill%27&VerticalAlignment=%27top%27&Format=%27$mimeType%27';
    
    return '$baseUrl(\'$id\')/%24value?$params';
  }
  
  /**
    Render Image element
  **/
  override function render() : ReactElement {
    var imgUrl = getImageUrl(props.data.Id, props.data.ImageMimeType);
    var imgCss = {
      'backgroundImage' : 'url(\"$imgUrl\")'
    };
    var className = 'image';
    
    if (Type.typeof(props.className) != TNull) {
      className += ' ' + props.className;
    }
    
    return React.createElement('div', { className: 'image ${props.className}', style: imgCss });
  }
}

/**
  Typing for the Image props
**/
typedef ImageProps = {
  data: ImageModel,
  ?className: String
}