package components.image;

/**
  Typing for the Image data
**/
typedef ImageModel = {
  Id : String,
  ImageMimeType : String
}