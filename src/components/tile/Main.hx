package components.tile;

import react.React;
import react.ReactDOM;
import react.ReactComponent;

import Config;
import utils.api.Main;
import components.tile.Model;
import components.image.Main;

/**
  Component for the rail Tile
**/
class Tile extends react.ReactComponentOfProps<TileProps> {
  public function new(props : TileProps) : Void {
    super(props);
  }
  
  /**
    Render content of the Tail
  **/
  private function renderContent() : Array<ReactElement> {
    var titles : Array<ReactElement> = [];
    
    if (Reflect.hasField(props.data, 'Competition') && Type.typeof(props.data.Competition) != TNull) {
      titles.push(React.createElement('div', { key: 'competition-title', className: 'tile__competition-title'}, props.data.Competition.Title));
    }
    
    titles.push(React.createElement('div', { key: 'title', className: 'tile__title' }, props.data.Title));
    titles.push(React.createElement('div', { key: 'description', className: 'tile__desc'}, props.data.Description));
    
    return titles;
  }
  
  /**
    Render tail image and container for the content
  **/
  private function renderElements() : Array<ReactElement> {    
    return [
      React.createElement(Image, { key: 'img', className: 'tile__img', data: props.data.Image }),
      React.createElement('div', { key: 'content', className: 'tile__content' }, renderContent())
    ];
  }
  
  /**
    Render Tile element
  **/
  override function render() : ReactElement {
    return React.createElement('div', { className: 'tile' }, renderElements());
  }
}

/**
  Typing for the Tile props
**/
typedef TileProps = {
  data : TileModel
}