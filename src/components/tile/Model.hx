package components.tile;

import components.image.Model;

/**
  Typing for the Tile data
**/
typedef TileModel = {
  Id : String,
  Title : String,
  Description : String,
  Image : ImageModel,
  Competition : TileCompetitionModel
}

/**
  Typing for the Tile competition data
**/
typedef TileCompetitionModel = {
  Title : String
}