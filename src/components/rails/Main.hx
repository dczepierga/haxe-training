package components.rails;

import react.React;
import react.ReactDOM;
import react.ReactComponent;

import Config;
import utils.api.Main;
import components.rails.Model;
import components.rail.Main;
import components.rail.Model;

/**
  Component for the Rails containing list of Rail
**/
class Rails extends react.ReactComponentOfPropsAndState<RailsProps, RailsState> {
  public function new(props : RailsProps) : Void {
    super(props);
    
    state = {
      rails: []
    };
  }
  
  /**
    Fetch rails data based on group id
  **/
  override function componentDidMount () : Void {
    var params : Map<String, String> = [
      'groupId' => props.groupId
    ];
    
    Api.loadService('Rails', Config.serviceRailsVersion, params).then(onData);
  }
  
  /**
    Set state with data when received
  **/
  public function onData(data : RailsModel) : Void {
    setState({ rails: data.Rails });
  }
  
  /**
    Render list with Rail components
  **/
  public function renderRails() : Array<ReactElement> {
    return state.rails.map(function (item) {
      return React.createElement(Rail, { key: item.Id, railId: item.Id });
    });
  }
  
  /**
    Render Rails element with all children
  **/
  override function render() : ReactElement {
    return React.createElement('div', { className: 'rails' }, renderRails());
  }
}

/**
  Typing for the Rails props
**/
typedef RailsProps = {
  groupId : String
}

/**
  Typing for the Rails state
**/
typedef RailsState = {
  rails : Array<RailModel>
}