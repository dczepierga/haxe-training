package components.rails;

import components.rail.Model;

/**
  Typing for the Rails data
**/
typedef RailsModel = {
  Rails : Array<RailModel>
};