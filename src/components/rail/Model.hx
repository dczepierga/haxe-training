package components.rail;

import components.tile.Model;

/**
  Typing for the Rail data
**/
typedef RailModel = {
  Id : String,
  Title : String,
  Tiles : Array<TileModel>
}