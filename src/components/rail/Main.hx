package components.rail;

import react.React;
import react.ReactDOM;
import react.ReactComponent;

import Config;
import utils.api.Main;
import components.rail.Model;
import components.tile.Main;
import components.tile.Model;

/**
  Component for the Rail with Tiles
**/
class Rail extends react.ReactComponentOfPropsAndState<RailProps, RailState> {
  public function new(props : RailProps) : Void {
    super(props);
    
    state = {
      title: '',
      tiles: []
    };
  }
  
  /**
    Fetch rail data based on rail id
  **/
  override function componentDidMount () : Void {
    var params : Map<String, String> = [
      'id' => props.railId,
      'LanguageCode' => Api.getLanguage(),
      'Country' => Api.getCountry()
    ];
    
    Api.loadService('Rail', Config.serviceRailVersion, params).then(onData);
  }
  
  /**
    Set state with data when received
  **/
  public function onData(data : RailModel) : Void {
    setState({ title: data.Id, tiles: data.Tiles });
  }
  
  /**
    Render title elements
  **/
  public function renderTitle() : ReactElement {
    return React.createElement('div', { key: 'title', className: 'rail__title' }, state.title);
  }
  
  /**
    Render list of Tile components
  **/
  public function renderTiles() : ReactElement {
    var tiles : Array<ReactElement> = state.tiles.map(function (item) {
      return React.createElement(Tile, { key: item.Id, data: item });
    });
    
    return React.createElement('div', { key: 'tiles', className: 'rail__tiles' }, tiles);
  }
  
  /**
    Render Rail element with all childrens
  **/
  override function render() : ReactElement {
    return React.createElement('div', { className: 'rail' }, [
      renderTitle(),
      renderTiles()
    ]);
  }
}

/**
  Typing for the Rail props
**/
typedef RailProps = {
  railId : String
}

/**
  Typing for the Rail state
**/
typedef RailState = {
  title : String,
  tiles : Array<TileModel>
}