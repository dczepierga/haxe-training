/**
  App configration
**/
class Config {
  // Startup data
  static public var urlStartup : String = 'https://isl.dazn.com/misl/v1/Startup?%24format=json';
  static public var urlStartupData : String = '{"LandingPageKey":"generic","Languages":"en-GB,en-US,en","Platform":"web","Manufacturer":"","PromoCode":"","Version":"1.6.0"}';
  
  // Service versions
  static public var serviceRailsVersion : String = 'v1';
  static public var serviceRailVersion : String = 'v1';
  static public var serviceImgVersion : String = 'v1';
}
