import react.React;
import react.ReactDOM;
import js.Browser;

import layouts.Home;
import utils.api.Main;

/**
  Main class for the app
**/
class App {
  public static inline function main() : Void {
    // initilize API to fetch startup data
    Api.startup().then(function (data) {
      ReactDOM.render(React.createElement(Home), Browser.document.getElementById('app'));
    });
  }
}
