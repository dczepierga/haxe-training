package utils.api;

/**
  Typing for the Api startup data
**/
typedef ApiModel = {
  ServiceDictionary: Map<String, ApiServiceModel>,
  Region: ApiRegionModel
}

/**
  Typing for the Api startup service data
**/
typedef ApiServiceModel = {
  Versions: Map<String, ApiServiceVersionModel>
}

/**
  Typing for the Api startup service version data
**/
typedef ApiServiceVersionModel = {
  ServicePath: String
}

/**
  Typing for the Api startup region data
**/
typedef ApiRegionModel = {
  Country: String,
  Language: String
}