package utils.api;

/**
  Enum for the request methods
**/
enum RequestMethod {
  GET;
  POST;
}