package utils.api;

import haxe.Http;
import haxe.Json;
import promhx.Deferred;
import promhx.Promise;

import Config;
import utils.api.Model;
import utils.api.Enums;

/**
  Class for to the communicate with API
**/
class Api {
  public static var instance(default, null) : Api = new Api();
  public static var data : ApiModel;
  
  private function new () : Void {}
  
  /**
    Prepare HTTP request
  **/
  private static function prepHttp(url : String, ?params : Map<String, String>, ?method : RequestMethod) {
    var http = new haxe.Http(url);
    
    http.setHeader('Content-Type', 'application/json');
    
    if (Type.typeof(method) == TNull) {
      method = GET;
    }
    
    if (method == GET) {
      if (Type.typeof(params) == TNull) {
        params = new Map();
      }
      params.set('$$format', 'json');
      
      for (key in params.keys()) {
        var value : String;
        if (StringTools.startsWith(key, '$')) {
          value = params[key];
        } else {
          value = '\'${params[key]}\'';
        }

        http.addParameter(key, value);
      }
    }
    
    return http;
  }
  
  /**
    Call startup endpoint to fetch all needed data
  **/
  public static function startup () : Promise<Dynamic> {
    var deffered = new Deferred<Dynamic>();
    var http = prepHttp(Config.urlStartup, null, POST);
    
    http.setPostData(Config.urlStartupData);
    
    http.onData = function(response : String) {
      data = haxe.Json.parse(response);
      
      deffered.resolve(data);
    }
    
    http.onError = function(response : String) {
      trace('Utils.Api: Request error', response);
      deffered.throwError(response);
    }
    
    http.request();
    
    return deffered.promise();
  }
  
  /**
    Return region country code
  **/
  public static function getCountry() : String {
    return data.Region.Country;
  }
  
  /**
    Return region language code
  **/
  public static function getLanguage() : String {
    return data.Region.Language;
  }
  
  /**
    Get service url from startup data
  **/
  public static function getServiceUrl(name : String, version : String = 'v1') : String {
    if (Reflect.hasField(data.ServiceDictionary, name)) {
      var versions : ApiServiceModel = Reflect.field(data.ServiceDictionary, name).Versions;
      
      if (Reflect.hasField(versions, version)) {
        var version : ApiServiceVersionModel = Reflect.field(versions, version);
        
        return version.ServicePath;
      }
    }
    
    return null;
  }
  
  /**
    Make HTTP call for given service based on startup data
  **/
  public static function loadService (service : String, version : String, ?params : Map<String, String>) : Promise<Dynamic> {
    var url : String = getServiceUrl(service, version);
    var deffered = new Deferred<Dynamic>();
    
    if (url.length > 0) {
      var http = prepHttp(url, params);
      
      http.onData = function(response : String) {
        deffered.resolve(haxe.Json.parse(response));
      }
      
      http.onError = function(response : String) {
        trace('Utils:Api: Request error', response);
        deffered.throwError(response);
      }
      
      http.request();
      
      return deffered.promise();
    }
    
    return null;
  }
}