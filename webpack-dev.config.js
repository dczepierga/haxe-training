const webpack = require('webpack');
const webpackConfig = require('./webpack-common.config');

const devServerPort = 8080;

webpackConfig.devtool = 'cheap-eval-source-map';

console.log(webpackConfig.entry);

webpackConfig.plugins.push(
  new webpack.HotModuleReplacementPlugin()
);

webpackConfig.module.rules.push({
  test: /\.scss$/i,
  use: [
    'style-loader', 
    'css-loader', 
    'sass-loader'
  ],
});

webpackConfig.devServer = {
  contentBase: './src/public',
  hot: true,
  port: devServerPort,
  inline: true
};

module.exports = webpackConfig;
