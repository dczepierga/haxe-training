# Haxe Training
Training app written in Haxe - connects to DAZN API and render homepage rails.

# Overview
* Sandboxed Haxe installation available only to npm scripts.
* App using DAZN startup endpoint to identify URL for all services.
* Files organized around features
* Webpack build with HMR support

## Requirements
* NodeJS 6.9.0 or above
* NPM 3.0.0 or above

## Installation
`npm install` to install all dependencies

After install all dependencies following commands are available:

* `npm run build` - prepare dist version of the app for the production (`bin` directory)
* `npm run build:haxe` - compile haxe code to the JavaScript
* `npm run build:webpack` - prepare app to run in the browser
* `npm run dev` - prepare dev version of the app and run webserver with HMR (http://localhost:8080/)
* `npm run dev:haxe` - run haxe watchify
* `npm run dev:webpack` - run webpack dev server
* `npm run docs` - generate documentation (`docs` directory)

## Dependencies
* React (https://github.com/facebook/react)
* Normalize - SCSS version (https://github.com/JohnAlbin/normalize-scss)
* Promhx (https://github.com/jdonaldson/promhx)

## Dev dependencies
* Webpack (https://webpack.github.io/)
* Node Sass (https://github.com/sass/node-sass)
* Haxe (http://haxe.org/)
* npm-haxe - node wrapper of Haxe (https://github.com/HaxeFoundation/npm-haxe)
* haxe-watchify (https://github.com/lucamezzalira/haxe-watchify)
