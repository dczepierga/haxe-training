const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: [
    path.join(__dirname, 'tmp', 'script.js'),
    path.join(__dirname, 'src', 'sass', 'style.scss')
  ],
  output: {
    path: path.join(__dirname, 'bin'),
    filename: 'script.js',
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './src/public/index.html',
    }),
  ],
  module: {
    rules: []
  }
};
