const webpack = require('webpack');
const webpackConfig = require('./webpack-common.config');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const extractSASS = new ExtractTextPlugin({
  filename: 'style.css'
});

webpackConfig.plugins.push(
  new CleanWebpackPlugin(['bin']),
  new CopyWebpackPlugin([{ from: 'src/public' }]),
  new webpack.optimize.UglifyJsPlugin({
    compress: {
      warnings: false,
    },
  }),
  extractSASS
);

webpackConfig.module.rules.push({
  test: /\.scss$/i,
  loader: extractSASS.extract(['css-loader', 'sass-loader']),
});

module.exports = webpackConfig;